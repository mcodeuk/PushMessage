﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;


namespace PushMessage
{
    static class CommandLine
    {
        static private Boolean helpneeded = false;
        static private Boolean showCopyRight = false;
               
        static Boolean paramErrors=false;

        public enum ErrorCodes
        {
            NoError,
            HelpRequired,
            WebCallFailed,
            ParamErrors,
            CopyRightNeeded
        } 

        static public CommandParser CommandLineOptions()
        {
            var parser = new CommandParser("PushMessage v1.0 - for sending PushOver messages");



            parser.Argument("d","debug","Provide debug information",CommandArgumentFlags.HideInAvailable | CommandArgumentFlags.HideInUsage, (p,v) =>
                {
                    Output.debugmode = true;
                });

            parser.Argument("a", "authtoken", "Authentication Token", CommandArgumentFlags.TakesParameter | CommandArgumentFlags.Required,
                (p, v) => { PushOver.token = v; });

            parser.Argument("u", "usersecret", "User Secret", CommandArgumentFlags.TakesParameter | CommandArgumentFlags.Required,
                (p, v) => { PushOver.userSecret = v; });

            
            parser.Argument("t", "title", "Title of the PushOver Message", CommandArgumentFlags.TakesParameter | CommandArgumentFlags.Required, 
                (p, v) => { PushOver.messageTitle = v; });
            
            parser.Argument("m", "message", "Message to send to PushOver", CommandArgumentFlags.TakesParameter | CommandArgumentFlags.Required, 
                (p, v) => { PushOver.messageText = v; });

            parser.Argument("s", "sound", "Sound to use", CommandArgumentFlags.TakesParameter,
                (p, v) => { PushOver.sound = v; });

            parser.Argument("c", "commandfile", "File name containing Command Line Options. Each line must take the form longcommandname=value or longcommandname",
                CommandArgumentFlags.TakesParameter | CommandArgumentFlags.FlushCommand, (p, v) =>
                {                                        
                    List<String> newcommands = LoadCommandFile(v);
                    if (newcommands.Count > 0)
                    {
                        parser.AddArguments(newcommands.ToArray());
                    }
                });

            parser.Argument("r", "replace", "Text Replacements on Message Title and Text of form OLDTEXT=NEWTEXT", CommandArgumentFlags.TakesParameter,
                (p, v) =>
                {
                    String[] elems = v.Split(new char[] { '=' }, 2);
                    if (elems.Count() != 2)
                    {
                        Output.WriteLine("Error : Replacement Ignored as no value : " + v);
                    }
                    else
                    {
                        if (PushOver.replacements == null)
                        {
                            PushOver.replacements = new List<Tuple<Boolean, string, string>>();
                        }
                        PushOver.replacements.Add(new Tuple<Boolean, string, string>(false, elems[0], elems[1]));
                    }
                });
            parser.Argument("R", "Replace", "As for replace command but using Strict case", CommandArgumentFlags.TakesParameter,
                (p, v) =>
                {
                    String[] elems = v.Split(new char[] { '=' }, 2);
                    if (elems.Count() != 2)
                    {
                        Output.WriteLine("Error : Replacement Ignored as no value : " + v);
                    }
                    else
                    {
                        if (PushOver.replacements == null)
                        {
                            PushOver.replacements = new List<Tuple<Boolean, string, string>>();
                        }
                        PushOver.replacements.Add(new Tuple<Boolean, string, string>(true, elems[0], elems[1]));
                    }
                });
            

            parser.Argument("q", "quiet", "Quiet Mode - Prevents any data being written to Console", CommandArgumentFlags.None,
                (p, v) =>
                    {
                        Output.SetQuietMode();
                    });
            parser.Argument("o", "output", "Output file name, sends Console info to the file specified", CommandArgumentFlags.TakesParameter, 
                (p, v) =>
                    {
                        Output.OpenOutputFile(v);
                    });
            
            parser.Argument("i", "info", "Display Copyright Information", (p, v) =>
            {
                showCopyRight = true;
            });
            parser.Argument("?", "help", "Display this Help Text", (p, v) =>
            {
                Help(parser);
            });

            return parser;
        }

        static private void Help(CommandParser parser)
        {
            helpneeded = true;
            Output.WriteLine(parser.GetHelp());
            Output.WriteLine("");
            Output.WriteLine("Note : Command Line parameters are processed left to right and can be repeated as required");
            Output.WriteLine("       For those parameters with a singular meaning the last value will be taken");
            Output.WriteLine("");
            Output.WriteLine("A default Config file will be loaded before any other parms are processed if detected at");
            Output.WriteLine("Local AppData folder - HomeGrown\\PushMessage in the file - Default.cfg");
            Output.WriteLine("or when running on Unix based systems in /etc/default/pushmessage");
        }

        static public ErrorCodes ProcessCommandLine(String[] args)
        {
            ErrorCodes rc = ErrorCodes.NoError;                   

            CommandParser parser = CommandLineOptions();
            if (args.Count() == 0)
            {
                Help(parser);
                rc = ErrorCodes.HelpRequired;
                //String[] helpme = new String[] { "-?" };
                //parser.Parse(helpme);
            }
            else
            {
                OperatingSystem os = Environment.OSVersion;
                Boolean hasAppData = false;
                String appdata = "";
                if (os.Platform == PlatformID.Win32NT || os.Platform == PlatformID.Win32S || os.Platform == PlatformID.Win32Windows || os.Platform == PlatformID.WinCE)
                {
                    appdata = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"HomeGrown\PushMessage\Default.cfg");
                }
                else
                {
                    appdata = "/etc/default/pushmessage";
                }
                if (File.Exists(appdata))
                {
                    hasAppData = true;
                    List<String> newcommands = LoadCommandFile(appdata);
                    if (newcommands.Count > 0)
                    {
                        parser.AddArguments(newcommands.ToArray());
                    }
                }
                parser.AddCommandLineArguments();
                parser.Parse();
                Output.Debug("Operating System : " + String.Format("{0}", os.Platform));
                if (hasAppData) Output.Debug("Defaults Loaded from : " + appdata);
                if (helpneeded)
                {
                    rc = ErrorCodes.HelpRequired;
                }
                else
                {
                    if (showCopyRight)
                    {
                        rc = ErrorCodes.CopyRightNeeded;
                    }
                    else
                    {
                        if (parser.MissingRequiredCommands.Count() > 0)
                        {
                            foreach (String command in parser.MissingRequiredCommands)
                            {
                                Output.WriteLine("Error : --" + command + " is required but was not supplied");
                            }
                            paramErrors = true;
                        }

                        if (paramErrors)
                        {
                            rc = ErrorCodes.ParamErrors;
                        }
                        else
                        {
                            if (!PushOver.SendMessage())
                            {
                                rc = ErrorCodes.WebCallFailed;
                            }
                        }
                    }
                }
            }
            return rc;
        }

        private static List<String> LoadCommandFile(String commandFile)
        {
            List<String> newcommands = new List<string>();
            if (File.Exists(commandFile))
            {
                Output.Debug("Reading Command File : " + commandFile);
                String[] lines = File.ReadAllLines(commandFile);
            
                foreach (String line in lines)
                {
                    if (line.StartsWith("#")) continue;
                    String tline = line.Trim();
                    if (tline.Length == 0) continue;
                    String[] elems = tline.Split(new char[] { '=' }, 2);
                    switch (elems.Count())
                    {
                        case 1:
                            Output.Debug("Adding Option : " + tline);
                            newcommands.Add("--" + tline);
                            break;
                        case 2:
                            Output.Debug("Adding Option : " + elems[0] + " with Parameter : " + elems[1]);
                            newcommands.Add("--" + elems[0] + " " + elems[1]);
                            break;
                        default:
                            Console.WriteLine("Invalid Command File Value : " + tline);
                            break;
                    }
                }
            }
            else 
            {
                Output.WriteLine("Error : Command File : " + commandFile + " does not exist!");
            }
            return newcommands;
        }
    }
}
