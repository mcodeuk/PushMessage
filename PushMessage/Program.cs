﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;

namespace PushMessage
{
    class Program
    {
        static int Main(string[] args)
        {
            int returncode = 0;
            CommandLine.ErrorCodes rc = CommandLine.ProcessCommandLine(args);
            switch(rc)
            {
                case CommandLine.ErrorCodes.NoError:
                    Output.WriteLine("PushMessage - Message Successfully Sent");                    
                    break;
                case CommandLine.ErrorCodes.HelpRequired:                 //   Help was requested                    
                    break;
                case CommandLine.ErrorCodes.CopyRightNeeded:
                    Assembly _assembly = Assembly.GetExecutingAssembly();
                    Output.WriteLine("Copyright Messages for included source code is listed below");
                    Output.WriteLine("");
                    using (StreamReader tstream = new StreamReader(_assembly.GetManifestResourceStream("PushMessage.CommandParserCopyright.txt")))
                    {
                        while (!tstream.EndOfStream)
                        {
                            Output.WriteLine(tstream.ReadLine());
                        }
                    }
                    break;
                case CommandLine.ErrorCodes.ParamErrors:
                    returncode = 4;
                    Output.WriteLine("PushMessage - Aborted due to parameter errors!");
                    break;
                case CommandLine.ErrorCodes.WebCallFailed:
                    Output.WriteLine("PushMessage - Send Error : " + PushOver.errorMessage);
                    Output.WriteLine("PushMessage - Message Failed to Send!");
                    returncode = 8;
                    break;
                default:
                    Output.WriteLine(String.Format("PushMessage - Terminated with Error Code : {0}", rc));
                    returncode = 16;
                    break;
            }
#if DEBUG
            Output.WriteLine(String.Format("Exiting with Code : {0}", returncode));
            Console.ReadKey();    
#endif        
            return returncode;
        }
    }
}
