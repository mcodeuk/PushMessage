﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;

namespace PushMessage
{
    public static class PushOver
    {
       
        public static String messageTitle;
        public static String messageText;
        public static String userSecret;
        public static String token;
        public static List<Tuple<Boolean,String,String>> replacements;        
        public static String sound = "magic";
        public static int requestTimeout = 5000;
        public static int requestOperationTimeout = 15000;
        private static String pushOverURL = "https://api.pushover.net/1/messages.json";
        public static String errorMessage;
        public static String responseMessage;
        public static Boolean SendMessage()
        {
            HttpWebRequest request = HttpWebRequest.Create(pushOverURL) as HttpWebRequest;
            request.PreAuthenticate = false;
            request.Timeout = requestTimeout;
            request.ReadWriteTimeout = requestOperationTimeout;
            if (replacements != null)
            {
                foreach (Tuple<Boolean, String, String> todo in replacements)
                {
                    if (todo.Item1)
                    {
                        messageText = Regex.Replace(messageText, todo.Item2, todo.Item3, RegexOptions.IgnoreCase);
                        messageTitle = Regex.Replace(messageTitle, todo.Item2, todo.Item3, RegexOptions.IgnoreCase);
                    }
                    else
                    {
                        messageText = messageText.Replace(todo.Item2, todo.Item3);
                        messageTitle = messageTitle.Replace(todo.Item2, todo.Item3);
                    }
                }
            }

            String urlencoded = EncodeMessage("token",token,"user",userSecret,"title",messageTitle,"sound",sound,"message",messageText);
            var data = Encoding.ASCII.GetBytes(urlencoded);
            try
            {
                request.ContentLength = data.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                request.Method = "POST";
                
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                errorMessage = null;
                try 
	            {	        
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
		            {
                        responseMessage = reader.ReadToEnd();
                    }
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return true;
                    }
                    else
                    {
                        errorMessage = responseMessage;
                        responseMessage = null;
                        return false;
                    }
	            }
	            catch (Exception ex)
	            {
                    errorMessage = ex.Message;		            
	            }                
                return true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                responseMessage = null;
                return false;
            }
        }

        private static String EncodeMessage(params String[] parms)
        {
            List<String> encodedparms = new List<string>();
            int m = parms.Length;
            int i = 0;
            while (i < m)
            {
                encodedparms.Add(parms[i] + "=" + Uri.EscapeDataString(parms[i + 1]));
                i = i + 2;
            }
            return String.Join("&", encodedparms);
        }
    }
}
