﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PushMessage
{
    public static class Output
    {
        static public Action<string> WriteLine = Console.WriteLine;
        static public Action<string> Debug = _DebugConsole;
        static public Action<string> Write = Console.Write;
        static private StreamWriter ofile;
        static public Boolean debugmode = false; 

        static public void OpenOutputFile(String filename)
        {
            if (filename != null && filename.Length > 0)
            {
                ofile = new StreamWriter(filename, false);
                ofile.AutoFlush = true;
                Debug("PushMessage Collecting to Output File : " + filename);
                WriteLine = _WriteLine;
                Debug = _DebugLine;
                Write = _Write;
            }
            else
            {
                WriteLine("WARNING : Cannot change output to file as no filename supplied!");
            }
        }
        static private void _WriteLine(String msg)
        {
            ofile.WriteLine(String.Format("{0} ", DateTime.Now) + msg);
        }

        static private void _Write(String msg)
        {
            ofile.Write(msg);
        }

        static public void SetQuietMode()
        {
            WriteLine = _QuietLine;
            Write = _Quiet;
        }
        static private void _QuietLine(String msg)
        {

        }

        static private void _Quiet(String msg)
        {

        }
        static private void _DebugConsole(String msg)
        {
            if (debugmode) Console.WriteLine(String.Format("{0} ", DateTime.Now) + "DEBUG " + msg);
        }
        static private void _DebugLine(String msg)
        {
            if (debugmode) ofile.WriteLine(String.Format("{0} ", DateTime.Now) + "DEBUG " + msg);
        }
    }
}
