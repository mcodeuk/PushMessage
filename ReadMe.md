# PushMessage #

The PushMessage application was written to allow a simple method of sending PushOver notifications from a Windows Command Line.

It is written in C# and for details of the API used please reference [Pushover Api](https://pushover.net/api).

This is a console application built for a minimum .Net Framework 4.

The CommandParser utility was imported courtesy of Christopher Hahn. Modifications have been made to this to enable the use of Configuration files to be processed in sequence with the Command Line parameters.

